package com.ltg.insuranceservice.insurancepolicy

import com.ltg.insuranceservice.vo.InsurancePolicyResultObject
import com.ltg.insuranceservice.vo.InsurancePolicyVO

interface InsurancePolicyService {

    /**
     * Create new insurance policy
     */
    fun createPolicy(policyVO: InsurancePolicyVO?): InsurancePolicyResultObject

    /**
     * get all insurance policy
     */
    fun getALl(): InsurancePolicyResultObject

    /**
     * Find the insurance policy by id
     */
    fun findById(id: Int?): InsurancePolicyResultObject

    /**
     * Update the insurance policy
     */
    fun updatePolicy(policyVO: InsurancePolicyVO?): InsurancePolicyResultObject

    /**
     * Delete the insurance policy
     */
    fun delete(id: Int?): InsurancePolicyResultObject
}