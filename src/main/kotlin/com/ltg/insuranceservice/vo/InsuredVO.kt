package com.ltg.insuranceservice.vo

import com.fasterxml.jackson.annotation.JsonProperty
import com.ltg.insuranceservice.entity.InsurancePolicy
import com.ltg.insuranceservice.entity.Insurer
import java.io.Serializable

class InsuredVO: Serializable {
    var id: Int? = null

    @JsonProperty("start_date")
    var startDate: String? = null

    var status: String? = null

    var insurancePolicy: InsurancePolicyVO? = null

    var insurer: InsurerVO? = null

}