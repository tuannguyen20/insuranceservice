package com.ltg.insuranceservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class InsuranceServiceApplication

fun main(args: Array<String>) {
    runApplication<InsuranceServiceApplication>(*args)
}
