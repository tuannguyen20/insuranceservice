package com.ltg.insuranceservice.repository

import com.ltg.insuranceservice.entity.Insurer
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface InsurerRepository: JpaRepository<Insurer, Int> {
}