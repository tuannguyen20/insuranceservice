package com.ltg.insuranceservice.insured

import com.ltg.insuranceservice.converter.InsuredConverter
import com.ltg.insuranceservice.entity.InsurancePolicy
import com.ltg.insuranceservice.entity.Insured
import com.ltg.insuranceservice.entity.Insurer
import com.ltg.insuranceservice.repository.InsurancePolicyRepository
import com.ltg.insuranceservice.repository.InsuredRepository
import com.ltg.insuranceservice.repository.InsurerRepository
import com.ltg.insuranceservice.util.DateTimeUtil
import com.ltg.insuranceservice.vo.InsuredResultObject
import com.ltg.insuranceservice.vo.InsuredVO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.ArrayList

@Service
class InsuredServiceImpl: InsuredService {

    @Autowired
    private lateinit var policyRepository: InsurancePolicyRepository

    @Autowired
    private lateinit var insurerRepository: InsurerRepository

    @Autowired
    private lateinit var insuredRepository: InsuredRepository

    override fun attach(policyId: Int?, insurerId: Int?): InsuredResultObject {

        if (policyId == null) {
            return InsuredResultObject(null, InsuredResultObject.StatusCode.INVALID_PARAM)
        }

        if (insurerId == null) {
            return InsuredResultObject(null, InsuredResultObject.StatusCode.INVALID_PARAM)
        }

        val policyOpt: Optional<InsurancePolicy> = policyRepository.findById(policyId)
        if (policyOpt.isEmpty) {
            return InsuredResultObject(null, InsuredResultObject.StatusCode.POLICY_NOT_FOUND)
        }

        val insurerOpt: Optional<Insurer> = insurerRepository.findById(insurerId)
        if (policyOpt.isEmpty) {
            return InsuredResultObject(null, InsuredResultObject.StatusCode.POLICY_NOT_FOUND)
        }

        val startDate = DateTimeUtil.getDateTime(System.currentTimeMillis())
        var insured = Insured(null, startDate, "SUCCESS")
        insured.insurer = insurerOpt.get()
        insured.insurancePolicy = policyOpt.get()
        insured.policyId = policyId
        insured.insurerId = insurerId

        insuredRepository.save(insured)

        val vo = InsuredConverter.convertToVO(insured)
        return InsuredResultObject(ArrayList(listOf(vo!!)), InsuredResultObject.StatusCode.SUCCESS)
    }

    override fun detach(policyId: Int?, insurerId: Int?): InsuredResultObject {
        if (policyId == null) {
            return InsuredResultObject(null, InsuredResultObject.StatusCode.INVALID_PARAM)
        }

        if (insurerId == null) {
            return InsuredResultObject(null, InsuredResultObject.StatusCode.INVALID_PARAM)
        }

        var insured = insuredRepository.find(policyId, insurerId)
        if (insured == null) {
            return InsuredResultObject(null, InsuredResultObject.StatusCode.INVALID_PARAM)
        }

        insuredRepository.delete(insured)
        return InsuredResultObject(null, InsuredResultObject.StatusCode.SUCCESS)
    }

    override fun findByPolicy(policyId: Int?): InsuredResultObject {
        if (policyId == null) {
            return InsuredResultObject(null, InsuredResultObject.StatusCode.INVALID_PARAM)
        }

        val insureds = insuredRepository.findByPolicy(policyId)
        val data = ArrayList<InsuredVO>()
        for (i in insureds) {
            val vo = InsuredConverter.convertToVO(i)
            data.add(vo!!)
        }

        return InsuredResultObject(data, InsuredResultObject.StatusCode.SUCCESS)
    }

    override fun findByInsurer(insurerId: Int?): InsuredResultObject {
        if (insurerId == null) {
            return InsuredResultObject(null, InsuredResultObject.StatusCode.INVALID_PARAM)
        }

        val insureds = insuredRepository.findByInsured(insurerId)
        val data = ArrayList<InsuredVO>()
        for (i in insureds) {
            val vo = InsuredConverter.convertToVO(i)
            data.add(vo!!)
        }

        return InsuredResultObject(data, InsuredResultObject.StatusCode.SUCCESS)
    }
}